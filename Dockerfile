FROM registry.gitlab.com/finlaydag33k/dashboard/nginx:latest

# Add our Nginx config
COPY ./config/nginx.conf /etc/nginx/nginx.conf

# Add the app files
COPY ./dist /var/www

EXPOSE 80

STOPSIGNAL SIGTERM

CMD ["nginx", "-g", "daemon off;"]
