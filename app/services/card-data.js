import Service from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class CardDataService extends Service {
  @tracked data = {
    name: 'John Doe',
    id: this.generateDocumentId(),
    fedi: {
      username: 'john.doe',
      instance: 'awesome.instance.tld',
    },
    issued: '???',
    expiry: '???',
    dob: {
      year: 1970,
      month: 1,
      day: 1,
    },
    flexitem: {
      header: 'Discord',
      value: 'Discord#0000',
    },
    pronoun: {
      singular: 'He',
      plural: 'Him',
    },
    colors: {
      background: '#002047',
      foreground: '#005ccc',
      variables: '#FFFFFF',
    },
    image: '',
  };

  constructor() {
    super(...arguments);

    // Get the issued date and expiry
    const now = new Date();
    this.data.issued = now
      .toLocaleDateString('en-us', {
        day: 'numeric',
        year: 'numeric',
        month: 'short',
      })
      .toUpperCase();
    this.data.expiry = new Date(now.setFullYear(now.getFullYear() + 5))
      .toLocaleDateString('en-us', {
        year: 'numeric',
        month: 'short',
      })
      .toUpperCase();
  }

  updateValue(name, value) {
    // eslint-disable-next-line ember/classic-decorator-no-classic-methods
    this.set(`data.${name}`, value);
    // eslint-disable-next-line no-self-assign
    this.data = this.data;
    console.log(this.data);
  }

  generateDocumentId() {
    let result = '';
    let letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let numbers = '0123456789';

    // Generate 3 letters
    for (let i = 0; i < 3; i++) {
      result += letters.charAt(Math.floor(Math.random() * 26));
    }

    // Generate 3 numbers
    for (let i = 0; i < 5; i++) {
      result += numbers.charAt(Math.floor(Math.random() * 10));
    }

    // Generate 3 letters
    for (let i = 0; i < 3; i++) {
      result += letters.charAt(Math.floor(Math.random() * 26));
    }

    return result;
  }
}
