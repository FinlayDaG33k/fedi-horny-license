import Component from '@glimmer/component';
import { inject as service } from '@ember/service';

export default class CardComponent extends Component {
  @service('card-data') card;
}
