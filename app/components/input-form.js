import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

export default class InputFormComponent extends Component {
  @service('card-data') card;

  @action
  update(event) {
    this.card.updateValue(event.target.name, event.target.value);
  }

  @action
  image(event) {
    let reader = new FileReader();
    reader.onloadend = () => {
      this.card.updateValue('image', reader.result);
    };
    reader.readAsDataURL(event.target.files[0]);
  }

  @action
  newId(e) {
    e.preventDefault();
    this.card.updateValue('id', this.card.generateDocumentId());
  }

  @action
  export() {
    if (this.card.data.image === '') {
      alert('Please upload an image before exporting!');
      return;
    }

    const png = new Promise((resolve, reject) => {
      try {
        // Get the card SVG
        const card = document.getElementById('card-svg');

        // Parse the XML
        let xml = new XMLSerializer().serializeToString(card);

        // Make it base64
        const based64 = `data:image/svg+xml;base64,${btoa(xml)}`;

        // Create a new image to hold our data
        const img = new Image();

        // Draw image to canvas
        let canvas = document.createElement('canvas');
        let context = canvas.getContext('2d');
        canvas.width = 1280;
        canvas.height = 720;

        img.onload = () => {
          // draw image with SVG data to canvas
          context.drawImage(img, 0, 0, 1280, 720);

          // Resolve with data URL
          resolve(canvas.toDataURL());
        };

        img.src = based64;
      } catch (e) {
        reject(`Failed to convert to PNG: ${e}`);
      }
    });

    png
      .then((res) => {
        const downloadLink = document.createElement('a');
        downloadLink.href = res;
        downloadLink.download = `fedi-horny-license-${this.card.data.id}.png`;
        downloadLink.click();
      })
      .catch((error) => {
        alert('Could not export to PNG! (see console for more information)');
        console.error(error);
      });
  }
}
