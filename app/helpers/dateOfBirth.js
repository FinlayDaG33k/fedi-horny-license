import { helper } from '@ember/component/helper';

function dateOfBirth([year, month, day]) {
  return new Date(`${month}/${day}/${year}`)
    .toLocaleDateString('en-us', {
      day: 'numeric',
      year: 'numeric',
      month: 'short',
    })
    .toUpperCase();
}

export default helper(dateOfBirth);
