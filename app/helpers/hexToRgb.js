import { helper } from '@ember/component/helper';

function hexToRgb([hex]) {
  hex = hex.replace('#', '');
  let data = hex.match(/.{1,2}/g);
  let red = parseInt(data[0], 16);
  let green = parseInt(data[1], 16);
  let blue = parseInt(data[2], 16);

  return `${red},${green},${blue}`;
}

export default helper(hexToRgb);
